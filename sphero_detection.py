#!/usr/bin/env python
import rospy  # rospy for the subscriber
import pdb

from sensor_msgs.msg import Image, CompressedImage  # ROS Image message
from geometry_msgs.msg import PoseArray, Pose

from std_msgs.msg import Float32, ColorRGBA

from cv_bridge import CvBridge, CvBridgeError  # ROS Image message -> OpenCV2 image converter
import time
import cv2  # OpenCV2 for saving an image
import numpy as np
from math import *
import pickle

# Instantiate CvBridge
bridge = CvBridge()


class SpheroImage:
    def __init__(self):
        self.thrh = 20
        self.thrs = 50
        self.thrv = 70

        self.agents_number = 5

        self.height = 480
        self.width = 640
        self.margin = 40

        # self.true_height = 1.52   # unit is meter
        # self.true_width = 2.44    # unit is meter

        self.true_height = 7.00 * 0.3 # unit is mosaic
        self.true_width = 10.00 * 0.3 # unit is mosaic
        self.sphero = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        # self.sphero = [[0, 0, 0, 0]] * self.agents_number
        self.v_x = 15 * [7*[0]]
        self.v_y = 15 * [7*[0]]
        self.v_x_p = 7 * [0]
        self.v_y_p = 7 * [0]
        self.sphero_trace = np.zeros((self.agents_number, 10, 2), np.float16)
        self.sphero_radius = 8
        self.heading_noise = 0
        self.search_area = 50
        self.setup = False
        self.sphero_setup = True
        self.Refresh = False

        self.first_time_detection = 'YES'
        self.first_time_detection_counter = 1
        self.first_time_detection_timer = 0

        self.image = np.zeros((self.height, self.width, 3), np.uint8)
        self.img = np.zeros((self.height, self.width, 3), np.uint8)
        self.image_median = np.zeros((self.height, self.width, 3), np.uint8)
        self.boundaries = []
        self.abs_pos = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        self.landmark = []
        self.mask = np.zeros((self.height, self.width, 1), np.uint8)
        self.frequency = 0
        self.frequency_process = 0
        self.initial_time = time.time()
        self.initial_time_process = time.time()

        self.sphero_input_vel_x = 0
        self.sphero_input_vel_y = 0
        self.estimated_position_x = 0
        self.estimated_position_y = 0
        self.initial_kalman = True
        self.R = [[0.035, 0], [0, 3.5]]
        self.P = self.R

        with open("/home/hossein/catkin_ws/src/sphero_image_processing/scripts/landmarks.txt", "rb") as fp:
            self.landmark = pickle.load(fp)
        print(self.landmark)

        # cv2.line(self.img, , , , , (25, 250, 100), 1)

        # Define image topic
        cv2.namedWindow('images')
        cv2.waitKey(1)

        # Set up subscriber and define its callback (compressed_topic, CompressedImage, compressed_callback)
        rospy.Subscriber("/decompressed", Image, self.image_callback)
        # Set up subscriber and define its callback (Velocity Pose2D massage)
        # rospy.Subscriber("/cmd_vel", PoseArray, self.velocity_callback)

        self.LED1 = rospy.Publisher('/set_color1', ColorRGBA)
        self.back_LED1 = rospy.Publisher('/set_back_led1', Float32)
        self.heading1 = rospy.Publisher('/set_heading1', Float32)

        self.LED2 = rospy.Publisher('/set_color2', ColorRGBA)
        self.back_LED2 = rospy.Publisher('/set_back_led2', Float32)
        self.heading2 = rospy.Publisher('/set_heading2', Float32)

        self.LED3 = rospy.Publisher('/set_color3', ColorRGBA)
        self.back_LED3 = rospy.Publisher('/set_back_led3', Float32)
        self.heading3 = rospy.Publisher('/set_heading3', Float32)

        self.LED4 = rospy.Publisher('/set_color4', ColorRGBA)
        self.back_LED4 = rospy.Publisher('/set_back_led4', Float32)
        self.heading4 = rospy.Publisher('/set_heading4', Float32)

        self.LED5 = rospy.Publisher('/set_color5', ColorRGBA)
        self.back_LED5 = rospy.Publisher('/set_back_led5', Float32)
        self.heading5 = rospy.Publisher('/set_heading5', Float32)

        self.LED6 = rospy.Publisher('/set_color6', ColorRGBA)
        self.back_LED6 = rospy.Publisher('/set_back_led6', Float32)
        self.heading6 = rospy.Publisher('/set_heading6', Float32)

        self.LED7 = rospy.Publisher('/set_color7', ColorRGBA)
        self.back_LED7 = rospy.Publisher('/set_back_led7', Float32)
        self.heading7 = rospy.Publisher('/set_heading7', Float32)

        self.position = rospy.Publisher('/position', PoseArray)
        self.pose_msg = PoseArray()
        i = 1
        while 1:
            self.heading1.publish(i)
            self.heading2.publish(i)
            self.heading3.publish(i)
            self.heading4.publish(i)
            self.heading5.publish(i)
            self.heading6.publish(i)
            self.heading7.publish(i)

            if cv2.waitKey(50) == ord('h'):
                break

    def distance(self, xi, yi, xii, yii):
        sq1 = (xi-xii)*(xi-xii)
        sq2 = (yi-yii)*(yi-yii)
        return sqrt(sq1 + sq2)

    def rotate(self, l, x):
        return l[-x:] + l[:-x]

    def set_initial_settings(self):
        if self.sphero_setup is True:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED1.publish(0)

            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED2.publish(0)

            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED3.publish(0)

            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED4.publish(0)

            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED4.publish(0)

            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED4.publish(0)

            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            # self.back_LED4.publish(0)
        self.sphero_setup = False
        time.sleep(0.3)

    def sphero_show(self, sphero_number):

        if sphero_number == 0:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 1:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 2:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 3:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 4:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 5:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.9)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.0)
        if sphero_number == 6:
            self.LED1.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED2.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED3.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED4.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED5.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED6.publish(a=1.0, r=0.0, g=0.0, b=0.0)
            self.LED7.publish(a=1.0, r=0.0, g=0.0, b=0.9)

    def sphero_identification(self):
        for i in range(self.agents_number):
            self.sphero_show(i)
            self.sphero[i] = self.cluster_detection(1, detection_mode='IDENTIFICATION')


    def positioning(self):
        print("\n")

        self.pose_msg = PoseArray()
        for i in range(self.agents_number):
            pose = Pose()

            x = self.sphero[i][0]
            y = self.sphero[i][1]
            scalar = self.distance(self.landmark[1][0], self.landmark[1][1], self.landmark[0][0], self.landmark[0][1])
            mpp = self.true_width / scalar
            d1 = self.distance(x, y, self.landmark[0][0], self.landmark[0][1]) * mpp
            d2 = self.distance(x, y, self.landmark[1][0], self.landmark[1][1]) * mpp
            d3 = self.distance(x, y, self.landmark[2][0], self.landmark[2][1]) * mpp

            h = self.true_height
            w = self.true_width

            sx = (d1 + d3 + h) / 2
            sy = (d1 + d2 + w) / 2
            self.abs_pos[i][0] = (2 / h) * sqrt(abs(sx * (sx - d1) * (sx - d3) * (sx - h)))
            self.abs_pos[i][1] = (2 / w) * sqrt(abs(sy * (sy - d1) * (sy - d2) * (sy - w)))

            print("Agent # %s" % int(i+1))
            print("     Absolute X position: %s" % self.abs_pos[i][0])
            print("     Absolute Y position: %s" % self.abs_pos[i][1])
            pose.position.x = self.abs_pos[i][0]
            pose.position.y = self.abs_pos[i][1]
            self.pose_msg.poses.append(pose)

    def velocity_prediction(self):

        for j in range(self.agents_number):
            if self.distance(self.sphero[j][0], self.sphero[j][1], self.sphero[j][2], self.sphero[j][3]) >= 0:
                self.v_x[j] = self.rotate(self.v_x[j], 1)
                self.v_y[j] = self.rotate(self.v_y[j], 1)
                self.v_x[j][0] = self.sphero[j][2] - self.sphero[j][0]
                self.v_y[j][0] = self.sphero[j][3] - self.sphero[j][1]
                self.v_x_p[j] = 0
                self.v_y_p[j] = 0
                for i in range(len(self.v_x[j])):
                    self.v_x_p[j] += self.v_x[j][i]
                    self.v_y_p[j] += self.v_y[j][i]
                self.v_x_p[j] = self.v_x_p[j] / len(self.v_x[j])
                self.v_y_p[j] = self.v_y_p[j] / len(self.v_x[j])

    def capture_area_color(self, event, x, y, flags, param):

        if event == cv2.EVENT_LBUTTONUP and self.setup is True and len(self.landmark) < 3:
            self.landmark.append([x, y])
            cv2.circle(self.img, (x, y), self.sphero_radius, (30, 240, 170), 2)
            print(self.landmark)
            if len(self.landmark) == 3:

                # Saving Landmarks
                with open("/home/hossein/catkin_ws/src/sphero_image_processing/scripts/landmarks.txt", "wb") as fp:
                    pickle.dump(self.landmark, fp)

                self.setup = False

        # if event == cv2.EVENT_MBUTTONUP:
        #     self.setup = False
        #     print("You have captured %s sphero(s)" % slen(self.sphero))

        elif event == cv2.EVENT_LBUTTONDBLCLK and self.setup is False:
            hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
            # hsv = self.image
            h = 0
            s = 0
            v = 0
            interest_pixels_num = 0

            # Average between colors around the click-point
            for py in range(0, self.height):
                for px in range(0, self.width):
                    if self.distance(px, py, x, y) < self.sphero_radius:
                        interest_pixels_num += 1
                        h += hsv[py, px, 0]
                        s += hsv[py, px, 1]
                        v += hsv[py, px, 2]
            mean_color_h = h / interest_pixels_num
            mean_color_s = s / interest_pixels_num
            mean_color_v = v / interest_pixels_num

            print("Color (%s, %s, %s) (HSV) captured at x = %s and y = %s. " % (mean_color_h, mean_color_s, mean_color_v,
                                                                             x, y))
            time.sleep(0.5)
            cv2.circle(self.img, (x, y), self.sphero_radius, (220, 90, 170), -1)

            # Store the captured color
            # self.sphero.append([int(x), int(y), int(mean_color_h), int(mean_color_s), int(mean_color_v)])

            # Append the new thresholds
            upper_hue = mean_color_h + self.thrh
            lower_hue = mean_color_h - self.thrh
            upper_sat = mean_color_s + 1*self.thrs
            lower_sat = mean_color_s - 1*self.thrs
            upper_val = mean_color_v + 4*self.thrv
            lower_val = mean_color_v - self.thrv
            if upper_hue > 180:
                upper_hue = 180
            if lower_hue < 0:
                lower_hue = 0
            if upper_sat > 255:
                upper_sat = 255
            if lower_sat < 0:
                lower_sat = 0
            if upper_val > 255:
                upper_val = 255
            if lower_val < 0:
                lower_val = 0
            self.boundaries.append(([lower_hue, lower_sat, lower_val], [upper_hue, upper_sat, upper_val]))
            print(self.boundaries)
            time.sleep(0.5)

    def image_callback(self, msg):
        # print("Received an image!")
        self.new_time = time.time()
        time_elapsed = self.new_time - self.initial_time
        self.frequency = 1/time_elapsed
        self.initial_time = time.time()

        try:
            # Convert your ROS Image message to OpenCV2
            cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            print(e)
        else:
            self.image = cv2.medianBlur(cv2_img, 9)
            self.img = cv2_img
            # print("im in callback______________________________________________")
            # Illustrate zone of interest
            # cv2.rectangle(self.img, (0 + self.margin, 0 + self.margin),
            #               (self.width - self.margin, self.height - self.margin), (0, 210, 0), 1)
            # if self.first_time_detection == 'DURING':


            if self.first_time_detection == 'NO':

                self.cluster_detection(self.agents_number, detection_mode='NORMAL')



                if self.agents_number != 0 and len(self.landmark) == 3:
                    for i in range(self.agents_number):
                        # print(self.sphero)
                        cv2.circle(self.img, (int(self.sphero[i][0]), int(self.sphero[i][1])), self.sphero_radius, (0, 240, 230), 1)
                        cv2.putText(self.img, "Agent #%s" % str(i+1), (int(self.sphero[i][0]), int(self.sphero[i][1])-20),
                                                                             cv2.FONT_HERSHEY_SIMPLEX, 0.4, (50, 10, 0))
                        if abs(self.v_x_p[i]) + abs(self.v_y_p[i]) > 0:
                            x = 20 * self.v_x_p[i] / sqrt(self.v_x_p[i] * self.v_x_p[i] + self.v_y_p[i] * self.v_y_p[i])
                            y = 20 * self.v_y_p[i] / sqrt(self.v_x_p[i] * self.v_x_p[i] + self.v_y_p[i] * self.v_y_p[i])
                        else:
                            x=0
                            y=0
                        cv2.line(self.img, (int(self.sphero[i][0]), int(self.sphero[i][1])),
                                 (int(self.sphero[i][0] - x), int(self.sphero[i][1] - y)), (25, 250, 100), 2)

            # cv2.namedWindow("images", cv2.WINDOW_NORMAL)
            # cv2.imshow("images", self.img)
            # cv2.waitKey(1)
            # if len(self.boundaries) != 0:
            #     self.kalman_filter()

    def velocity_callback(self, msg):
        for i in range(self.agents_number):
            self.sphero_input_vel_x[i] = msg.x[i] * 0.8/255
            self.sphero_input_vel_y[i] = msg.y[i] * 0.8/255

    def cluster_detection(self, agent_numbers, detection_mode):
        # output = np.zeros((self.height, self.width, 3), np.uint8)
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)

        for (lower, upper) in self.boundaries:
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype="uint8")
            upper = np.array(upper, dtype="uint8")

            # find the colors within the specified boundaries and apply
            # the mask
            image = self.image
            self.mask = cv2.inRange(hsv, lower, upper)
            # output = cv2.bitwise_and(image, image, mask=mask)

            # K-Mean Cluster
            interested_point_cloud = np.transpose(np.nonzero(self.mask))
            Z = np.float32(interested_point_cloud)
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
            K = agent_numbers
            ret, label, center = cv2.kmeans(Z, K, None, criteria, 30, cv2.KMEANS_RANDOM_CENTERS)

            # Initial Update... So orders doesnt matter!
            if self.first_time_detection != 'NO' and detection_mode == 'NORMAL':

                self.first_time_detection = 'DURING'
                # self.sphero_identification()
                for k in range(self.agents_number):
                    self.sphero_show(k)
                    time.sleep(1)
                    temp = self.cluster_detection(1, detection_mode='IDENTIFICATION')
                    self.sphero[k][0] = temp[0]
                    self.sphero[k][1] = temp[1]
                    print("_________________________________________ x (px) : %s" % self.sphero[k][0])
                    print("_________________________________________ y (px) : %s" % self.sphero[k][1])
                    # self.sphero[k][0] = center[k][1]
                    # self.sphero[k][1] = center[k][0]

                    self.sphero[k][2] = self.sphero[k][0]
                    self.sphero[k][3] = self.sphero[k][1]

                self.sphero_setup = True
                self.set_initial_settings()
                self.first_time_detection = 'NO'



            # Match the clusters position with agents position
            elif detection_mode == 'NORMAL':

                occupied_clusters = []
                for p in range(agent_numbers):
                        # Linear prediction of agents position based on it's previews position.
                        # predicted_x = 2 * self.sphero[p][0] - self.sphero[p][2]
                        # predicted_y = 2 * self.sphero[p][1] - self.sphero[p][3]
                        predicted_x = self.sphero[p][0]
                        predicted_y = self.sphero[p][1]

                        # Choosing a large value for initial min_dist
                        min_dist = 2000
                        for j in range(agent_numbers):
                                dj = self.distance(predicted_x, predicted_y, center[j][1], center[j][0])
                                if dj < min_dist and j not in occupied_clusters:
                                    min_dist = dj
                                    closest = j

                        # Turn new positions to old
                        self.sphero[p][2] = self.sphero[p][0]
                        self.sphero[p][3] = self.sphero[p][1]

                        # Update new position with new detected cluster which is closest to our estimation.
                        if self.distance(self.sphero[p][0], self.sphero[p][1], self.sphero[p][2], self.sphero[p][3]) < 5:
                            self.sphero[p][0] = center[closest][1]
                            self.sphero[p][1] = center[closest][0]
                            occupied_clusters.append(closest)

            elif detection_mode == 'IDENTIFICATION':
                            return [center[0][1], center[0][0]]

    def kalman_filter(self):

        if self.initial_kalman is True:
            self.estimated_position_x = self.abs_pos[0][0]
            self.estimated_position_y = self.abs_pos[0][1]
            self.initial_kalman = False

        else:
            dt = 1/self.frequency

            # Calculate predicted positions
            x_p = self.estimated_position_x + dt * self.sphero_input_vel_x
            y_p = self.estimated_position_y + dt * self.sphero_input_vel_y

            # K = P * (P + R)^-1
            K = np.matmul(self.P, np.linalg.inv(np.add(self.P, self.R)))
            # P = (I - K) * P
            self.P = np.matmul(np.subtract([[1, 0], [0, 1]], K), self.P)

            predicted = [[x_p], [y_p]]
            measured = [[self.abs_pos[0][0]], [self.abs_pos[0][1]]]
            print(predicted)
            print(measured)
            [self.estimated_position_x, self.estimated_position_x] =\
                np.add(predicted, np.matmul(K, np.subtract(measured, predicted)))

            print(self.estimated_position_x)
            print(self.estimated_position_y)

    def main(self):

        r = rospy.Rate(60)

        self.new_time_process = time.time()
        time_elapsed_process = self.new_time_process - self.initial_time_process
        self.frequency_process = 1 / time_elapsed_process
        self.initial_time_process = time.time()


        # sphero_setup = True
        while not rospy.is_shutdown():
            self.new_time_process = time.time()
            time_elapsed_process = self.new_time_process - self.initial_time_process
            if time_elapsed_process != 0:
                self.frequency_process = 1 / time_elapsed_process
            self.initial_time_process = time.time()

            cv2.namedWindow("images", cv2.WINDOW_NORMAL)
            cv2.imshow("images", self.img)
            cv2.waitKey(1)

            cv2.imshow('mask', self.mask)
            cv2.waitKey(1)

            cv2.imshow('median', self.image)
            cv2.waitKey(1)


            if self.first_time_detection == 'YES':
                self.cluster_detection(self.agents_number, detection_mode='NORMAL')

            if self.first_time_detection == 'NO':
                self.velocity_prediction()

            if self.setup is True:
                print("\n" * 30)
                print("Landmark Setup Mode")
                print("Click on your Landmark #%s/3" % int(len(self.landmark)+1))

            if cv2.waitKey(10) == ord('r'):
                self.setup = True
                self.landmark = []

            cv2.setMouseCallback('images', self.capture_area_color)

            if len(self.landmark) == 3:
                self.positioning()

            print("Stream frequency: %s" % self.frequency)
            print("Process frequency: %s" % self.frequency_process)

            if self.sphero_setup is True:
                self.set_initial_settings()

            self.position.publish(self.pose_msg)

            r.sleep()


if __name__ == '__main__':

    rospy.init_node('sphero_detection')
    SI = SpheroImage()
    SI.main()

