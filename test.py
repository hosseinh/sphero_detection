import cv2
import numpy as np
from math import *

margin = 80
setup = 0
agents_number = 1
height = 480
width = 640


# Distance function
def distance(xi,yi,xii,yii):
    sq1 = (xi-xii)*(xi-xii)
    sq2 = (yi-yii)*(yi-yii)
    return sqrt(sq1 + sq2)


def capture_area_color(img, x, y, r):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h = 0
    s = 0
    v = 0
    interest_pixels_num = 0
    for py in range(0 + margin, height - margin):
        for px in range(0 + margin, width - margin):
            if distance(px, py, x, y) < r:
                interest_pixels_num += 1
                h += hsv[py, px, 0]
                s += hsv[py, px, 1]
                v += hsv[py, px, 2]
    mean_color_h = h / interest_pixels_num
    mean_color_s = s / interest_pixels_num
    mean_color_v = v / interest_pixels_num
    return [int(mean_color_h), int(mean_color_s), int(mean_color_v)]


im = cv2.imread("back.png")
print(capture_area_color(im, 100, 100, 20))
cv2.imshow("images", im)
cv2.waitKey(0)